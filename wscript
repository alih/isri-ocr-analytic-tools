VERSION = '0.1'
APPNAME = 'isri-ocr-evaluation-tools'

top = '.'
out = 'build'

def configure(conf):
    conf.load('gcc')
    conf.check(lib='m')
    conf.env.append_value('CFLAGS', ['-O2'])
    
def build(bld):
    bld.objects(target = 'lib',
                source = bld.path.ant_glob('src/*.c'))
    for i in bld.path.ant_glob('tools/*.c'):
        bld.program(target = i.name[:-2],
                    includes = 'src',
                    use = 'lib M',
                    source = [i])
    bld.install_files('${PREFIX}/share/man1', bld.path.ant_glob('tools/*.1'))

# vim: set ft=python:
